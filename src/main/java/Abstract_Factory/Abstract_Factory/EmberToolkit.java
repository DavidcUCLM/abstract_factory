package Abstract_Factory.Abstract_Factory;

public class EmberToolkit extends AbstractFactory {
	public CPU createCPU() {
	     return new EmberCPU();
	 }

	 public MMU createMMU() {
	     return new EmberMMU();
	 }
}
