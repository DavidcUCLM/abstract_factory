package Abstract_Factory.Abstract_Factory;

public class Client {
	public static void main(String[] args) {
	     AbstractFactory factory = AbstractFactory.getFactory(Architecture.EMBER);
	     CPU cpu = factory.createCPU();
	 }
}
